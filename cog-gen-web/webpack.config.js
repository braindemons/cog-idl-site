const path = require("path");
const WasmPackPlugin = require("@wasm-tool/wasm-pack-plugin");
const CleanWebpackPlugin = require('clean-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
    entry: "./index.js",
    output: {
        path: path.resolve(__dirname, "../static/cog-gen-web"),
        publicPath: "/cog-idl-site/cog-gen-web/",
        filename: "index.js",
    },
    plugins: [
        new CleanWebpackPlugin(),
        new WasmPackPlugin({
            crateDirectory: path.resolve(__dirname, ".")
        }),
        new MiniCssExtractPlugin({
            // Options similar to the same options in webpackOptions.output
            // both options are optional
            filename: '[name].css',
            chunkFilename: '[id].css',
        }),
    ],
    module: {
        rules: [
            {
                test: /\.css$/,
                use: [
                    { loader: MiniCssExtractPlugin.loader },
                    'css-loader',
                ],
            },
        ],
    },
    mode: "production"
};