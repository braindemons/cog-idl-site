import "codemirror/lib/codemirror.css";
import "codemirror/theme/material.css";
import "./index.css";

import "codemirror/mode/rust/rust";
import "codemirror/mode/clike/clike";

const sample_code =
`//! This is an example module, to show how Cog IDL works.

/// Greeter interface, has functions for greeting.
interface greeter {
    /// Says hello.
    fn hello();

    /// Says hello to a given person.
    fn hello_to(person: &cstr);
}

/// An example API registry.
interface registry {
    /// Sets an API in the registry.
    fn set(name: &cstr, api: *any);

    /// Gets an API in the registry by name.
    fn get(name: &cstr) -> *any;
}`;

// Load and set up the WASM
import("./pkg/cog_gen_web")
    .then(function(cog_gen_web) {
        let current_language = cog_gen_web.Language.C;

        // Create the input field
        let CodeMirror = require("codemirror");
        let editor = CodeMirror(document.getElementById("cog-web-input"), {
            mode: "rust",
            theme: "material",
            value: sample_code,
            lineNumbers: true,
            indentUnit: 4,
            tabSize: 4,
        });

        // Create the output field
        let output = CodeMirror(document.getElementById("cog-web-output"), {
            mode: "clike",
            theme: "material",
            value: cog_gen_web.generate(sample_code, current_language),
            lineNumbers: true,
            indentUnit: 4,
            tabSize: 4,
            readOnly: "nocursor",
        });

        // Update output when input changes
        editor.on("change", () => {
            let code = editor.getValue();
            let generated = cog_gen_web.generate(code, current_language);
            output.setValue(generated);
        });

        // Make the nav switch what language should be generated
        let c_button = document.getElementById("cog-web-c");
        let rust_button = document.getElementById("cog-web-rust");

        c_button.addEventListener("click", (e) => {
            e.preventDefault();

            c_button.classList.add("active");
            rust_button.classList.remove("active");

            current_language = cog_gen_web.Language.C;

            let code = editor.getValue();
            let generated = cog_gen_web.generate(code, current_language);
            output.setValue(generated);
            output.setOption("mode", "clike");
        });
        rust_button.addEventListener("click", (e) => {
            e.preventDefault();

            c_button.classList.remove("active");
            rust_button.classList.add("active");

            current_language = cog_gen_web.Language.Rust;

            let code = editor.getValue();
            let generated = cog_gen_web.generate(code, current_language);
            output.setValue(generated);
            output.setOption("mode", "rust");
        });

        // TODO: Send error lines from rust to here
        //editor.addLineClass(5, "background", "line-error");
    })
    .catch(console.error);