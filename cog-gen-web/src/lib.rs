use wasm_bindgen::prelude::wasm_bindgen;

#[wasm_bindgen]
pub fn generate(input: &str, language: Language) -> String {
    // Generate C for the source into output
    let result = cog_idl::load_module_from_str("greeter", &input);

    let output = match result {
        Ok(module) => {
            match language {
                Language::C => cog_gen_c::generate_header(&module, Some("myapi_")),
                Language::Rust => cog_gen_rust::generate_module_formatted(&module),
            }
        },
        Err(error) => format!("{}", error),
    };

    output
}

#[wasm_bindgen]
pub enum Language {
    C,
    Rust,
}
