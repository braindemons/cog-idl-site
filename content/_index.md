---
title: 'Cog IDL'
date: 2018-11-28T15:14:39+10:00
---

A tool for generating cross-language APIs.

## Try it Out

<div class="card text-left mb-2" id="cog-web-input">
</div>

<h2>
    <span class="oi" data-glyph="chevron-bottom" title="chevron bottom" aria-hidden="true" style="color:#313539"></span>
</h2>
<ul class="nav nav-pills mb-1">
    <li class="nav-item">
        <a class="nav-link active" id="cog-web-c" href="#">C</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="cog-web-rust" href="#">Rust</a>
    </li>
</ul>
<div class="card text-left mb-8" id="cog-web-output">
</div>

<script src="/cog-idl-site/cog-gen-web/index.js"></script>

## Want to know more?