---
title: "Getting Started"
date: 2019-04-21T22:41:15+02:00
draft: false
weight: 3
---

<div class="alert alert-warning" role="alert">
    Cog IDL's documentation is currently incomplete.
    Take a look at
    <a href="https://gitlab.com/braindemons/cog-idl/tree/master/examples">the examples</a>,
    and other documentation pages on how to use it.
</div>
