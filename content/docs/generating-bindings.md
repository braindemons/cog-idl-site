---
title: "Generating Bindings"
date: 2019-04-21T22:24:32+02:00
draft: false
weight: 4
---

Cog IDL includes official generators for multiple languages.
If you need to generate bindings for a language not yet supported, you can use the `cog-idl`
Rust library to parse Cog APIs.


## C

C headers for a Cog API can be generated using the command line tool `cog-gen-c`.

```bash
cog-gen-c \
    --api ./api \
    --headers ./include/mylib \
    --prefix mylib_
```

Use `--api` to specify the input API directory containing your .cog files, and `--headers` to
specify the directory to output header files to.

C does not have namespacing, making collisions more likely.
To avoid this, you can use `--prefix` to add a prefix to all generated identifiers.


## Rust

Rust modules for a Cog API can be generated using the command line tool `cog-gen-rust`.

```bash
cog-gen-rust \
    --api ./api \
    --module ./crates/mylib-api/src/generated
```

Use `--api` to specify the input API directory containing your .cog files, and `--module` to specify
the directory to output modules to.

The tool will not generate your entire cargo project, only module files.
This is to make it easier to add helpers, and language-specific code to your API bindings.
It's a good idea to add a "generated" directory to your "src" directory to put the generated files
into.
You can re-export the modules from your "lib.rs".