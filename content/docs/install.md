---
title: "Install Cog IDL"
date: 2019-02-11T19:27:37+10:00
weight: 2
---

Cog IDL's generator tools are available through [Cargo](https://crates.io/), the official Rust
package manager.
Follow the recommended install instructions for your platform on [Rust's install page](https://www.rust-lang.org/tools/install).

After you have installed Rust and Cargo, you can install the Cog IDL generation tools through cargo:

```bash
# To install the C generator
cargo install cog-gen-c-bin

# To install the Rust generator
cargo install cog-gen-rust-bin
```

These tools are also available as libraries for use in custom build systems.