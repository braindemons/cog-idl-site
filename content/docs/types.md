---
title: "Types"
date: 2019-04-26T20:08:57+02:00
weight: 5
---

<div class="alert alert-warning" role="alert">
    Cog IDL's type system is currently extremely incomplete, and a heavy work in progress.
    More features will be added later to support more complex APIs.
</div>

# Built-In Types

Type Name | Usage
----------|------
`cstr` | Null-terminated C string. This can only be used as a reference or pointer.
`any` | Non-specified type. Equivalent to a C `void`. This can only be used as a reference or pointer.


# Pointers and References

To have a function take a type by reference, rather than by value, you can prefix the type with
either `&` or `*`.

- `&` makes your type a reference.
    In Cog IDL this means your type has more strict lifetime requirements.
    A reference should only be treated as valid for the duration of the function call and no longer.
    Using this type where possible is preferred.

- `*` makes your type a raw pointer type.
    This is a generic pointer you may be used to in C.
    It has no explicit lifetime requirements.
    This means the pointer's validity has more compelex requirements that should be listed in the
    function's documentation.
    These semantics conflict with some other languages, and are prone to error.
    Avoid this where possible.
